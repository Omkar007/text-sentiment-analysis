package com.cloud.project;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

//Second Stage Mapper 
public class SecondParseMapper extends Mapper<Text,Text,Text,Text>{

	  @Override
	    protected void map(Text key, Text value, Context context)
	                    throws IOException, InterruptedException {
			//context.write(new Text(key),new Text(value));
			int indexlst=key.toString().lastIndexOf('#')+1;
			context.write(new Text(new Text( key.toString().substring(indexlst).trim())), new Text(String.valueOf(value)));
		  
	  }
	
}
