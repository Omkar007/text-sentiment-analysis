package com.cloud.project;

public class AbsaJson {
	private String id;
	private String text;
	
	public class AspectTerms {
		private String term;
		private String polarity;
		private String from; 
		private String to;
		public String getTo() {
			return to;
		}
		public void setTo(String to) {
			this.to = to;
		}
		public String getTerm() {
			return term;
		}
		public void setTerm(String term) {
			this.term = term;
		}
		public String getPolarity() {
			return polarity;
		}
		public void setPolarity(String polarity) {
			this.polarity = polarity;
		}
		public String getFrom() {
			return from;
		}
		public void setFrom(String from) {
			this.from = from;
		}
		
	};
	private AspectTerms aspectTerms[];
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public AspectTerms[] getAspectTerms() {
		return aspectTerms;
	}
	public void setAspectTerms(AspectTerms aspectTerms[]) {
		this.aspectTerms = aspectTerms;
	}
}
