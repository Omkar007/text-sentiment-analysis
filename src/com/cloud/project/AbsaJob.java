package com.cloud.project;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import java.io.InputStream;
import java.net.URL;
import net.sf.json.JSON;
import net.sf.json.xml.XMLSerializer;
import org.apache.commons.io.IOUtils;

public class AbsaJob {
	
	public final static String Laptops_Train = "Laptops_Train.json";
	public final static String SARCASM_REVIEW = "sarcasm.json";
	public final static String Restaurant = "Restaurant_Train.json";
	private static final String OUTPUT_PATH = "intermediate_output";	
	
	public static void main(String[] args) throws Exception {
		
		Job job = new Job();
		initJob(job);
		
		File f = new File("Output");
		FileUtils.deleteDirectory(f);
		job.setJarByClass(AbsaJob.class);
		
		//JSON json = doXMLtoJson("Laptops_Train.xml");
		//System.out.println(json);
	
		MultipleInputs.addInputPath(job, new Path(SARCASM_REVIEW), TextInputFormat.class);
		MultipleInputs.addInputPath(job, new Path(Restaurant), TextInputFormat.class);
		//Results will be stored in folder named Output in the root directory of the project
		FileOutputFormat.setOutputPath(job, new Path(OUTPUT_PATH));
		job.submit();
		job.waitForCompletion(true);
		
		   // Calling Second Parse Mapper and Reducer
		 Job job2=new Job();
         initCustom(job2);
         job2.setJarByClass(AbsaJob.class);
         FileInputFormat.setInputPaths(job2,new Path(OUTPUT_PATH));
         FileOutputFormat.setOutputPath(job2, new Path("Output"));
         job2.submit();
         job2.waitForCompletion(true);
	}
	
	public static void initJob(Job job) {
		
		job.setInputFormatClass(org.apache.hadoop.mapreduce.lib.input.TextInputFormat.class);
		
		//job.setMapperClass(com.cloud.project.ReviewAnalyzerMapper.class);
		job.setMapperClass(com.cloud.project.AbsaMapperAspect.class);
		job.getConfiguration().set("mapred.mapper.new-api", "true");
		
		job.getConfiguration().set("mapred.map.tasks", "3");
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);
		
		job.setPartitionerClass(org.apache.hadoop.mapreduce.lib.partition.HashPartitioner.class);
		
		job.setReducerClass(com.cloud.project.AbsaReducer.class);
		job.getConfiguration().set("mapred.reducer.new-api", "true");

		job.getConfiguration().set("mapred.reduce.tasks", "2");
		job.setOutputKeyClass(org.apache.hadoop.io.Text.class);
		job.setOutputValueClass(org.apache.hadoop.io.Text.class);

		job.setOutputFormatClass(org.apache.hadoop.mapreduce.lib.output.TextOutputFormat.class);
	}

    //Custom Init Method for initializing Second Mapper & Reducer
    public static void initCustom(Job job) {
    
    	org.apache.hadoop.conf.Configuration conf = job.getConfiguration();
    	job.setInputFormatClass(org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat.class);

    	// CG_MAPPER_HIDDEN
    	job.setMapperClass(com.cloud.project.SecondParseMapper.class);
    	job.getConfiguration().set("mapred.mapper.new-api", "true");

    	// CG_MAPPER
    	job.getConfiguration().set("mapred.map.tasks", "3");
    	job.setMapOutputKeyClass(org.apache.hadoop.io.Text.class);
    	job.setMapOutputValueClass(org.apache.hadoop.io.Text.class);

    	// CG_PARTITIONER_HIDDEN
    	job.setPartitionerClass(org.apache.hadoop.mapreduce.lib.partition.HashPartitioner.class);
    	
    	// CG_REDUCER_HIDDEN
    	job.setReducerClass(com.cloud.project.SecondParseReducer.class);
    	job.getConfiguration().set("mapred.reducer.new-api", "true");

    	// CG_REDUCER
    	job.getConfiguration().set("mapred.reduce.tasks", "2");
    	job.setOutputKeyClass(org.apache.hadoop.io.Text.class);
    	job.setOutputValueClass(org.apache.hadoop.io.Text.class);

    	// CG_OUTPUT_HIDDEN
    	job.setOutputFormatClass(org.apache.hadoop.mapreduce.lib.output.TextOutputFormat.class);

    	job.getConfiguration().set("", "");

    }
    
    public static JSON doXMLtoJson(String filename) 
    {
    	InputStream inputStream = null;
    	JSON objJson = null;
        try{
        	File f = new File(filename);
        	byte[] encoded = Files.readAllBytes(f.toPath());
    		String xmlString = new String(encoded,  StandardCharsets.UTF_8);
            objJson = new XMLSerializer().read(xmlString);
            
        }catch(Exception e){
            e.printStackTrace();
        }finally{
        	try {
                if (inputStream != null) {
                    inputStream.close();
                }
            } catch (IOException ex) {}
        }
        return objJson;
    }	    
}
