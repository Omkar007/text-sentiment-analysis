package com.cloud.project;

import java.io.IOException;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

//Second Stage Reducer
public class SecondParseReducer extends Reducer<Text,Text,Text,Text>{

	   @Override
	    protected void reduce(Text key, Iterable<Text> values, Context context)
	                        throws IOException, InterruptedException {
		   Double val=0.0;
		   String polarity;
		   String score;
			//System.out.println("Key   "+key);
			for (Text link : values) {		
			//	System.out.println("score "+link);
				score=link.toString().substring(link.toString().indexOf("(")+1,link.toString().indexOf(")"));
				//System.out.println("score "+score);
					val=val+Double.parseDouble(score);
			}
			if(val>0)
				polarity="POSITIVE";
			else
				polarity="NEGATIVE";
			  context.write(new Text(key), new Text("\tPolarity:" + polarity + "(" + val+ ")"));
	   }
	
}
