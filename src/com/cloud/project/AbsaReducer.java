package com.cloud.project;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.hash.Hash;

public class AbsaReducer extends Reducer<Text, Text, Text, Text> {
	
	private Set<String> goodWords = new HashSet<String>();
	private Set<String> badWords = new HashSet<String>();
	private Set<String> interestingWords = new HashSet<String>();
	
	protected void setup(Reducer<Text, Text, Text, Text>.Context context)
			throws IOException, InterruptedException{
		String word;
	
		FileReader badWordsReader = new FileReader("NegativeWords.txt");
		FileReader goodWordsReader = new FileReader("PositiveWords.txt");
		FileReader interestingWordsReader = new FileReader("InterestingWords.txt");
		
        BufferedReader badWordsBufferedReader = new BufferedReader(badWordsReader);
        BufferedReader goodWordsBufferedReader = new BufferedReader(goodWordsReader);
        BufferedReader interestingWordsBufferedReader = new BufferedReader(interestingWordsReader);
        
        while ((word = badWordsBufferedReader.readLine()) != null) {
			badWords.add(word);
		}
        
        while ((word = goodWordsBufferedReader.readLine()) != null) {
        	goodWords.add(word);
		}
        
        while ((word = interestingWordsBufferedReader.readLine()) != null) {
        	interestingWords.add(word);
		}
        
        badWordsBufferedReader.close();  
        goodWordsBufferedReader.close(); 
        interestingWordsBufferedReader.close(); 
	}
	
	@Override
	protected void reduce(Text asin, Iterable<Text> reviewWordLists, Context context)
			throws IOException, InterruptedException {
		
		double POSITIVE_SCORE = 0.0;
		double NEGATIVE_SCORE = 0.0;	
		String polarity;
		Set<String> interestingReviewWords = new HashSet<String>();
		Set<String> stemWords = new HashSet<String>();
	    HashMap<String,String> aspectSentimentMap=new HashMap<>();
		boolean isSarcasm=false;
		String reviewWords = "";
		for (Text wordList : reviewWordLists) {
			
			if(wordList.toString().contains("absaSarcasm")){
				isSarcasm=true;
				System.out.println("key "+asin.toString()+" found Sarcasm");
			}
			reviewWords = reviewWords + wordList;
    	}
		
		if(reviewWords.contains("absaSarcasm"))
		{
			isSarcasm = true;
			System.out.println("key "+asin.toString()+" found Sarcasm");
		}

		
		List<CardKeyword> keywordsList = KeywordsExtractor.getKeywordsList(reviewWords);
		for (final CardKeyword keyword : keywordsList) {
				
			
			for (String term : keyword.getTerms()) {
				if (goodWords.contains(term)) {
				//	System.out.println("GOOD : " + term);
					POSITIVE_SCORE += 1 + Math.log10(keyword.getFrequency());
					//if(interestingReviewWords.size() <= 20 && stemWords.add(keyword.getStem()) && interestingWords.contains(keyword.getStem())){
						interestingReviewWords.add(term + "(+)");
					//}
				}else if (badWords.contains(term)) {
					//System.out.println("BAD : " + term);
					NEGATIVE_SCORE += 1 + Math.log10(keyword.getFrequency());
					//if(interestingReviewWords.size() <= 20 && stemWords.add(keyword.getStem()) && interestingWords.contains(keyword.getStem()) ){
						interestingReviewWords.add(term + "(-)");
					//}
				}else if(keyword.getStem().startsWith("0")){
					//System.out.println("START WITH 0 : " + term + "::" + keyword + "::" + keyword.getStem());
					NEGATIVE_SCORE += 1 + Math.log10(keyword.getFrequency());
					//if(interestingReviewWords.size() <= 20 && stemWords.add(keyword.getStem())){
						interestingReviewWords.add(term + "(-)");
					//}
				}else{
					//if(interestingReviewWords.size() <= 20 && interestingWords.contains(keyword.getStem()) && stemWords.add(keyword.getStem())){
						//System.out.println("LAST LOOP : " + term + "::" + keyword + "::" + keyword.getStem());
						//interestingReviewWords.add(term + "(.)");
					//}
				}
			}
		}
		
		if(isSarcasm == true) {
			NEGATIVE_SCORE += 1 + Math.log10(2);
		}
		
		if(0 == (NEGATIVE_SCORE + POSITIVE_SCORE)) {		
			//polarity = "neutral";
			//context.write(new Text(asin + " Polarity:" + polarity), new Text("Words:" + interestingReviewWords));
		} else {
			double value = (POSITIVE_SCORE - NEGATIVE_SCORE) / (NEGATIVE_SCORE + POSITIVE_SCORE) ;
			
			if(value > 0)
				polarity = "positive";
			else
				polarity = "negative";
			
			context.write(new Text(asin + "\tPolarity:" + polarity + "(" + value + ")"), new Text("\tWords:" + interestingReviewWords));
			//context.write(new Text(aspectSentimentMap.get(asin.toString().substring(asin.toString().lastIndexOf('#')+1)) + "\tPolarity:" + polarity + "(" + aspectSentimentMap.get(asin).toString().substring(asin.toString().lastIndexOf('#')+1) + ")"), new Text(""));
		}
	}
	
	public HashMap<String, String> getAspectSentimentAnalysis(Text reviewKey,double scoreValue, HashMap<String, String>aspectSentimentMap){
		//System.out.println("reviewID: "+reviewKey);
		int index=reviewKey.toString().lastIndexOf('#');
		String aspect=reviewKey.toString().substring(index+1).trim();
	
		if(!aspectSentimentMap.containsKey(aspect)){
			System.out.println("aspect: "+aspect);
			aspectSentimentMap.put(aspect, String.valueOf(scoreValue));
		}
		else{
			String val=aspectSentimentMap.get(aspect);
			System.out.println("aspect "+aspect);
			Double score=0.0;
			if(scoreValue>0)
			score=scoreValue+Double.parseDouble(val);
			else
				score=scoreValue-Double.parseDouble(val);
		
			aspectSentimentMap.put(aspect, String.valueOf(score));
		}
		
		
		return aspectSentimentMap;
	}
	
}
