package com.cloud.project;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AbsaMapperAspect extends Mapper<LongWritable, Text, Text, Text>  {

	private Set<String> stopWords = new HashSet<String>();
	private Set<String> negationWords = new HashSet<String>();
	private Set<String> aspectWords = new HashSet<String>();
	private Set<String> positiveVerbPhrases = new HashSet<String>();
	private Set<String> positivePredicatePhrases = new HashSet<String>();
	private Set<String> negativePredicatePhrases = new HashSet<String>();
	private Set<String> sarcasText = new HashSet<String>();
	
	@Override
	protected void setup(Mapper<LongWritable, Text, Text, Text>.Context context)
	throws IOException, InterruptedException{
		String word;
		
		//Load Stop Words
		FileReader stopWordsReader = new FileReader("StopWords.txt");
		
        BufferedReader stopWordsBufferedReader = new BufferedReader(stopWordsReader);
        
        while ((word = stopWordsBufferedReader.readLine()) != null) {
        	stopWords.add(word);
		}
        
        stopWordsBufferedReader.close();  
        
        
        //Load Negation Words
		FileReader negationWordsReader = new FileReader("NegationWords.txt");
		
        BufferedReader negationWordsBufferedReader = new BufferedReader(negationWordsReader);
        
        while ((word = negationWordsBufferedReader.readLine()) != null) {
        	negationWords.add(word);
		}
        
        negationWordsBufferedReader.close();  
        
        //Load aspect Words
  		//FileReader aspectWordsReader = new FileReader("LaptopAspectWords.txt");
  		FileReader aspectWordsReader = new FileReader("RestaurantAspectWords.txt");
        BufferedReader aspectWordsBufferedReader = new BufferedReader(aspectWordsReader);
          
        while ((word = aspectWordsBufferedReader.readLine()) != null) {
          	aspectWords.add(word);
  		}
        aspectWordsBufferedReader.close();
        
     // start --- Added for Sarcasm Detection//
     		FileReader posVerbsReader = new FileReader("PositiveVerbs.txt");
     		FileReader posPhrasesReader = new FileReader("PositivePredicativeExpression.txt");
     		FileReader negPredicPhrases = new FileReader("NegativeExpressionPhrases.txt");
     		
            BufferedReader posVerbsBuffReader = new BufferedReader(posVerbsReader);
            BufferedReader posPhraseBuffReader = new BufferedReader(posPhrasesReader);
            BufferedReader negaContadictPhrases = new BufferedReader(negPredicPhrases);
     
            while ((word = posVerbsBuffReader.readLine()) != null) {
            	positiveVerbPhrases.add(word);
    		}
            
            while ((word = posPhraseBuffReader.readLine()) != null) {
            	positivePredicatePhrases.add(word);
    		}
            
            while ((word = negaContadictPhrases.readLine()) != null) {
            	negativePredicatePhrases.add(word);
    		}
            posVerbsBuffReader.close();
            posPhraseBuffReader.close();
            negaContadictPhrases.close();   		
     		
     		// end --- Added for Sarcasm Detection//

	}
	
	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		Gson gson = new GsonBuilder().create();
		
		//JSON to class
		AbsaJson json = gson.fromJson(value.toString(), AbsaJson.class);
		
		String reviewText =json.getText();
	
		//First Phase Sarcasm Detection
		Pattern regex = Pattern.compile("[$&#!?@]");
		Matcher matcher = regex.matcher(reviewText);
		if (matcher.find()){
		//System.out.println("sarcastic text found "+reviewText); 
		}
		else if(reviewText.startsWith("#sarcasm") || reviewText.endsWith("#sarcasm")){
			 System.out.println("sarcasm tag found "+reviewText); 	
		}
		
		boolean isSarcasm=false;
		//check if sarcastic positive word list is in review text
		for (String positiveVerbs : positiveVerbPhrases) {
		    int index=0;
			if(reviewText.contains(positiveVerbs)){
				index=reviewText.indexOf(positiveVerbs);
				String next=reviewText.substring(index);
			//	String sar[]=reviewText.substring(index).split(" ");
//				String comp="";
//				if(sar.length>=4){
//					comp=sar[1]+sar[2]+sar[3];	
//				}
				
				for(String negate:negativePredicatePhrases){
					Pattern regex1 = Pattern.compile(negate);
					Matcher matcher1 = regex1.matcher(next);
					if(matcher1.find()){
						isSarcasm=true;
						sarcasText.add("absaSarcasm "+reviewText);
						System.out.println("absaSarcasm "+reviewText);
						break;
					}
				}
			}
		}
	
		//Join negative words with next word
		for (String negationWord : negationWords) {
			reviewText = reviewText.replaceAll("[^A-Za-z,;\\.\\r?\\n ]", "").toLowerCase().replaceAll(negationWord + " ", negationWord + "-");
		}
		
		//Split into sub-strings to consider each aspect (punctuation)
		//String conjunctions = "";
		String [] stringArray = reviewText.split("[,;\\.\\r?\\n]|\\s(for|and|nor|but|or|yet|so)\\s");
        ArrayList<String> stringList = new ArrayList<>(Arrays.asList(stringArray));
        stringList.remove("");

		//@todo : split on conjunctions
        
        //Check if string contains aspect
        // Write (id#aspectWord, words[])
        for(String SubString : stringList) {
        	
        	//Split text into words
            String [] wordsArray = SubString.split(" ");
            ArrayList<String> wordsList = new ArrayList<>(Arrays.asList(wordsArray));
            
	        for (String aspectWord : aspectWords) {
	        	String aspectWordL = aspectWord.toLowerCase();
	        	if (wordsList.contains(aspectWordL)) {
	                
	                wordsList.remove(aspectWordL);
	                
	                //remove stop words from list
	                for (String stopWord : stopWords) {
	                	if (wordsList.contains(stopWord)) {
	               		 wordsList.removeAll(Collections.singleton(stopWord));//remove it
	                    }
	        		}
	                
	                //build string from word list
	                StringBuilder builder = new StringBuilder();
	                for (String word : wordsList) {
	                    builder.append(word + " ");
	                    
	                }
	                if(isSarcasm){
	                	builder.append("absaSarcasm");
	                }
	                String text = builder.toString();
	         
	                //write to intermediate output for reducer to process
	                context.write(new Text(json.getId() + "#" + aspectWordL), new Text(text));
	            }
			}
        }
	}	
}
