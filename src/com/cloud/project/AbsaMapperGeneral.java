package com.cloud.project;

import java.io.BufferedReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class AbsaMapperGeneral extends Mapper<LongWritable, Text, Text, Text>  {

	private Set<String> stopWords = new HashSet<String>();
	
	@Override
	protected void setup(Mapper<LongWritable, Text, Text, Text>.Context context)
	throws IOException, InterruptedException{
		String word;
		FileReader stopWordsReader = new FileReader("StopWords.txt");
		
        BufferedReader stopWordsBufferedReader = new BufferedReader(stopWordsReader);
        
        while ((word = stopWordsBufferedReader.readLine()) != null) {
        	stopWords.add(word);
		}
        
        stopWordsBufferedReader.close();  
        
	}
	
	@Override
	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		Gson gson = new GsonBuilder().create();
		
		//JSON to class
		Review rev = gson.fromJson(value.toString(), Review.class);
		
		String[] negationForms = {"not","dont","doesnt","havnt","couldnt","arent","cant", "couldnt",
				"didnt","hadnt", "hasnt","shouldnt","isnt","wasnt","werent","wont","wouldnt"};
		
		String reviewText = rev.getReviewText();
		System.out.println("inside mapper "+reviewText);
		//Join negative words with next word
		for (int i = 0; i < negationForms.length; i++) {
			reviewText = reviewText.replaceAll("[^A-Za-z ]", "").toLowerCase().replaceAll(negationForms[i] + " ", negationForms[i] + "-");
		}
		
		//Split text into words
        String [] wordsArray = reviewText.split(" ");
        ArrayList<String> wordsList = new ArrayList<>(Arrays.asList(wordsArray));
        
        //remove stop words from list
        for (String stopWord : stopWords) {
        	if (wordsList.contains(stopWord)) {
       		 wordsList.removeAll(Collections.singleton(stopWord));//remove it
            }
		}
        
        //build string from word list
        StringBuilder builder = new StringBuilder();
        for (String word : wordsList) {
            builder.append(word + " ");
        }
        String text = builder.toString();
        
        //write to intermediate output for reducer to process
        context.write(new Text(rev.getAsin()), new Text(text));
		//System.out.println(text);
	}	
}
